#!/bin/sh -e

VERSION=$2
TAR=../xslthl_$VERSION.orig.tar.gz
DIR=v$VERSION
TAG=$(echo "v$VERSION" | sed -re's/\./_/g')

svn export https://xslthl.svn.sourceforge.net/svnroot/xslthl/tags/${TAG}/ $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' --exclude 'examples' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
